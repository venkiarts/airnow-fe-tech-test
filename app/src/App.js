import React from 'react';
import InfoPage from "./Pages/InfoPage/InfoPage";
import { useDispatch } from "react-redux";
import { installedSdks, uninstalledSdks } from "./Utils/Api";
import { InstalledActions } from "./Features/InstalledSlice";
import { UninstalledActions } from "./Features/UninstalledSlice";
import { useEffect } from "react";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
    root: {
        display: 'flex',
        justifyContent: "center",
        marginTop:"50px",
        boxSizing: 'border-box'
    }
})
function App() {
    const dispatch = useDispatch();
    const classes = useStyles();

    useEffect(() => {
        // This can be fetch be API or only call API either page loaded or use click on installed or uninstalled tab.
        const fetchInstalledSdks = installedSdks.data.installedSdks;
        const fetchUninstalledSdks = uninstalledSdks.data.uninstalledSdks;
        dispatch(InstalledActions.getInstalledSdks({installedSdks: fetchInstalledSdks} ));
        dispatch(UninstalledActions.getUninstalledSdks({uninstalledSdks: fetchUninstalledSdks} ));
    },[dispatch]);

    return (
        <div className={classes.root}>
            <InfoPage />
        </div>
  );
}

export default App;
