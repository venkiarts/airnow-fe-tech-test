import {createSlice} from '@reduxjs/toolkit';

const initialState = {name: "installedState", installedSdks: []}

export const InstalledSlice = createSlice ({
    name: 'installedSlice',
    initialState: initialState,
    reducers: {
        getInstalledSdks(state, action) {
            state.installedSdks = action.payload.installedSdks
        }
    }
})

export const InstalledActions = InstalledSlice.actions;
export default InstalledSlice.reducer;