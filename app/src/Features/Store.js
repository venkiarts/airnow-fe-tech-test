import {configureStore} from '@reduxjs/toolkit';
import InstalledReducer from './InstalledSlice'
import UninstalledReducer from "./UninstalledSlice";


const store = configureStore({
    reducer : {
        installedSdks: InstalledReducer,
        uninstalledSdks: UninstalledReducer
    }
});

export default store;