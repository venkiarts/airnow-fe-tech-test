import { createSlice } from '@reduxjs/toolkit';

const initialState = {name: "uninstalledState", uninstalledSdks: []}

export const UninstalledSlice = createSlice ({
    name: 'uninstalledSlice',
    initialState: initialState,
    reducers: {
        getUninstalledSdks(state, action) {
            state.uninstalledSdks = action.payload.uninstalledSdks
        }
    }
})

export const UninstalledActions = UninstalledSlice.actions;
export default UninstalledSlice.reducer;