import * as React from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { groupByCategory, sortingData } from "../../Utils/DataHelper";
import Divider from "@mui/material/Divider";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import DataBlockComponent from "../Blocks/DataBlockComponent";
import { useStyles } from "../../Pages/InfoPage/InfoPageStyles";
import { useEffect, useState } from "react";
import { convert } from "../../Utils/TimeConverter";

export default function TabPanelComponent ({data, count, title, ...props})  {

    const classes = useStyles();
    const [latestUpdate, setLatestUpdate] = useState('');
    const sdkDataByCategory = groupByCategory(data);


    // will find the latest update by sorting latest date and return first one.
    const findLatestUpdate = (entries) => {
        const sortByLastSeenDate = sortingData(entries, 'by-date');
        let getFirstSortedDate = Object.values(sortByLastSeenDate)[0]?.lastSeenDate;
        return convert(getFirstSortedDate);
    };


    useEffect(() => {
        if(data) {
            const lastSeenDate = findLatestUpdate(data);
            setLatestUpdate(lastSeenDate);
        }
    }, [data])


    return (
        <Grid container>
            <Grid item lg={12} >
                <Box display="flex" justifyContent="space-between" alignItems="center"  mb={2}>
                    <Box>
                        <Typography data-testid="testDataHeading" variant="h5" gutterBottom>{title}</Typography>
                        <Typography variant="subtitle1" >Latest Update : {latestUpdate}</Typography>

                    </Box>
                    <Typography data-testid="testDataCount" component="h4" variant="h5" >{parseInt(count)}</Typography>
                </Box>
                <Divider />
                <List className={classes.listWrapper}>
                    {
                        Object.keys(sdkDataByCategory).map((key, index)  => (
                            <ListItem key={index}>
                                <DataBlockComponent key={index} data={{key, "sdks": sdkDataByCategory[key]}} />
                            </ListItem>
                        ))
                    }
                </List>
            </Grid>
        </Grid>
    )
}