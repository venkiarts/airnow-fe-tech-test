import * as React from 'react';
import timeConverter from "../../Utils/TimeConverter";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

export default function DataBlockComponent({data,...props}) {

    return (
        <Box data-test-id="dataBlock">
            <Typography variant="subtitle1" style={{textDecoration: 'underline'}}>{data.key}</Typography>
            {
                data.sdks.map((item) => (
                    <Typography component="h6" variant="subtitle1" key={item.id}>{item.name} &nbsp;
                        {timeConverter(item.firstSeenDate)}
                    </Typography>
                ))
            }
        </Box>
    )
}