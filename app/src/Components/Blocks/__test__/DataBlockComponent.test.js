import React from "react";
import { render, screen } from '@testing-library/react';
import DataBlockComponent from "../DataBlockComponent";

test("DataBlock renders without crashing", () => {
    const div = document.createElement("div");
    render(<DataBlockComponent data={{key:1, sdks: [{id:1, name: "Testing DataBlock", firstSeenDate: "22/10/2018 11:00:00"}]}} />, div);

    // expect(screen.getByRole('h6')).toHaveTextContent('1');

})