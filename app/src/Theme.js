import { createTheme } from '@mui/material/styles';
import './index.css';

const theme = createTheme({
    palette : {
        type: 'light',
        primary: {
            main: '#1683d8',
        },
        secondary: {
            main: '#121212',
            light:'rgba(255, 255, 255, 0.7)',
            dark: '#323232',
        }
    },
    typography: {
        fontFamily: "TektonProRegular",
        color: '#1683d8',
        h5: {
            fontWeight: 700,
            color: '#1683d8',
        },
        subtitle1: {
            fontWeight: 600,
            color: '#1683d8',
            letterSpacing: '0.09rem'
        }
    }
});

export default theme;