import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';
import {Provider} from "react-redux";
import store from "./Features/Store";

test('Testing basic app component', async () => {
  render(<Provider store={store}><App /></Provider>);
  expect(screen.getByText("Installed")).toHaveClass("Mui-selected");
  expect(screen.getByTestId("testDataHeading")).toHaveTextContent("Installed SDK\'s");

  fireEvent(
      screen.getByText('Uninstalled'),
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
      }),
  )
  expect(screen.getByText("Uninstalled")).toHaveClass("Mui-selected");
  expect(screen.getByTestId("testDataHeading")).toHaveTextContent("Uninstalled SDK\'s");

});
