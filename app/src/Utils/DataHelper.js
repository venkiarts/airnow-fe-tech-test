
// this will help us to all sdk's group by category
export const groupByCategory = (entry) => {
    const data = entry.reduce((group, entry) => {
        const { categories } = entry;
        categories.map((cat) => {
            group[cat] = group[cat] ?? [];
            return group[cat].push(entry);
        });
        return group;
    }, {});
    return data;
};


// this will help us reuse th sorting method for future sorting the entries
export const sortingData = (entries, sortingType) => {

    let arraySort = [...entries];
    switch (sortingType) {
        case 'a-z':
            arraySort = arraySort.sort((a,b) => {
                return a.name.localeCompare(b.name);
            })
            break;
        case 'z-a':
            arraySort = arraySort.sort((a,b) => {
                return b.name.localeCompare(a.name);
            })
            break;
        case 'by-date':
           arraySort = arraySort.sort((a,b) => {
                return new Date(b.lastSeenDate) - new Date(a.lastSeenDate);
            });
           break;
        case 'category':
            arraySort = arraySort.sort((a, b) => {
                return a.categories[0].localeCompare(b.categories[0]);
            })
            break;
        default:
    }

    return arraySort;
}