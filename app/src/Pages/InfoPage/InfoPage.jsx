import React from 'react'
import Box from "@mui/material/Box";
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import TabPanelComponent from "../../Components/Tabs/TabsPanelComponent";
import { useStyles } from "./InfoPageStyles";
import { useSelector } from "react-redux";
import { sortingData } from "../../Utils/DataHelper";


export default function InfoPage() {
    const [value, setValue] = React.useState("1");
    const classes = useStyles();
    const listOfInstalledSdks = useSelector(state => state.installedSdks.installedSdks);
    const listOfUninstalledSdks = useSelector(state => state.uninstalledSdks.uninstalledSdks);
    const installedSdksSortingByAlphabet = sortingData(listOfInstalledSdks, 'category');
    const uninstalledSdksSortingByAlphabet = sortingData(listOfUninstalledSdks, 'category');

    const navTabs = [
        {
            key: "1",
            label: "Installed",
            type: "installedSdk"
        },
        {
            key: "2",
            label: "Uninstalled",
            type: "uninstalledSdk"
        }
    ];
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Box sx={{ width: '80%', typography: 'body1' }} className={classes.root}>
            <TabContext value={value}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <TabList onChange={handleChange} aria-label="SDK's Tabs">
                        {
                            navTabs.map(item => (
                                <Tab date-testid="tabButtontest" disableFocusRipple disableRipple key={item.key} label={item.label} value={item.key} />
                            ))
                        }
                    </TabList>
                </Box>
                {
                    navTabs.map(item => (
                        <TabPanel key={item.key} value={item.key}>
                            {
                                item.type === 'installedSdk' &&
                                <TabPanelComponent
                                    title="Installed SDK's"
                                    count={installedSdksSortingByAlphabet.length}
                                    data={installedSdksSortingByAlphabet}
                                />

                            }
                            {
                                item.type === 'uninstalledSdk' &&
                                <TabPanelComponent
                                    title="Uninstalled SDK's"
                                    count={uninstalledSdksSortingByAlphabet.length}
                                    data={uninstalledSdksSortingByAlphabet}
                                />
                            }
                        </TabPanel>
                    ))
                }
            </TabContext>
        </Box>
    );
}