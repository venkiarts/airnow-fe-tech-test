import * as React from 'react';
import { makeStyles} from '@mui/styles';
import theme from "../../Theme";

export const useStyles = makeStyles( {
    root: {
        border: 0,
        borderRadius: 3,
        padding: '0 30px',
        '& .MuiTabs-root': {
            marginBottom: '30px'
        },
        '& .MuiTabs-flexContainer': {
            display: 'inline-block',
            border: '4px solid #1683d8',
            borderRadius: '50px',
            padding: '0px 20px',
            '& .MuiButtonBase-root': {
                fontFamily: 'TektonProBold',
                color: theme.palette.primary.main,
                fontWeight: 400,
                textTransform: 'capitalize',
                padding: '10px',
                fontSize: '24px'
            },
            '& > :first-child': {
                borderRight: '2px solid #1683d8'
            }
        },
        '& .MuiTabPanel-root': {
            border: '4px solid #1683d8',
            borderRadius: '5px',
            height: '470px',
            overflowY: 'auto'
        },
        '& .Mui-selected': {
            textDecoration: 'underline'
        },
        '& .css-1aquho2-MuiTabs-indicator': {
            display: 'none'
        },
        '& .MuiDivider-root': {
           height: '2px',
           backgroundColor: '#1683d8'
        }

    },
    listWrapper: {
        '-moz-column-count': 2,
        '-moz-column-gap': '20px',
        '-webkit-column-count': 2,
        '-webkit-column-gap': '20px',
        columnCount: 2,
        columnFill: 'balance',
        columnGap: '20px',
        width: '100%',
        columnSpan: 'none'
    }
});